# README #
This repository is intended as a help in the Smart Fabrics Hackathon.

#Bitalino Example

In order to pair the bitalino, the PIN is 1234.

##Dependencies
The bitalino Python API only works with python2.7.

###Dependency install
```python
pip2 install bitalino pyserial
```
###Only Linux
```bash
sudo apt-get install libbluetooth-dev
```
```python
pip2 install pybluez
```

If there is a problem connecting to the bitalino, please install the gnome bluetooth manager.

```bash
sudo apt-get install blueman
```

##Files
The files contained in this repository are the following: 

- example1.py 
- example2.py 
- blue.py

###example1.py
example1.py contains the example code to interact with the bitalino board.
In order to execute the example1.py you must have installed the bitalino API.

Usage of example1.py is the following:

**USAGE:** python example1.py {/dev/port_to_bitalino|COMx} {running_time [s] } {acq_channels} {sampling_rate} {nSamples} {path_to_file}

**EXAMPLE:** python example1.py COM3 10 \"1;2;3\" 1000 10 c:\\path\to\file\bitalino.txt

**EXAMPLE:** python example1.py /dev/ttyBITalino 10 \"1;2;3\" 1000 10 ~/data/bitalino.txt

The example will open the COM3 port, run for 10 seconds, sample the channels 1, 2 and 3 using a Sample Frequency of 1 KHz and 10 samples per channel, it will save the data to the bitlino.txt file.

#Flora Example

##Dependencies
In order for example2.py to work the following [code](https://bitbucket.org/marcmateumateus/hack_arduino) must be loaded to the Flora board; a tutorial explaining how, can be found [here](https://learn.adafruit.com/getting-started-with-flora/overview). A tutorial on how to use the Flora Board with Arduino IDE can be found [here](https://learn.adafruit.com/add-boards-arduino-v164/setup)

###example2.py
example2.py contains a simple way to interact with the neopixels in the flora board using bluetooth.

**USAGE:** python example2.py {port|name}

- Windows **EXAMPLE:** python example2.py COM4
- Linux **EXAMPLE:** python example2.py Flora_1; Be carefull, linux uses the actual name of the device not the port...
- Mac **EXAMPLE:** python example2.py /dev/tty.bluetoothport'