import platform

import serial
import time
import sys

def send_data(inst, data, ser):
    if ser:
        inst.write(data)
    else:
        inst.send(data)

if len(sys.argv) != 2:
    print 'USAGE: python example2.py {port|name}'
    print 'Windows EXAMPLE: python example2.py COM4'
    print 'Linux EXAMPLE: python example2.py Flora_1; Be carefull, linux uses the actual name of the device not the port...'
    print 'Mac EXAMPLE: python example2.py /dev/tty.bluetoothport'
    exit(-2);

if platform.system() == 'Darwin' or platform.system() == 'Windows':
    bp = serial.Serial(str(sys.argv[1]), timeout=0, baudrate=9600, xonxoff=False, rtscts=False, dsrdtr=False)
    serial = True
else:
    from blue import blue
    bp = blue()
    dev = bp.scanDevices(str(sys.argv[1]))
    serial = False
    if len(dev) > 0:
        bp.connectDevice(dev[0]['addr'])
    else:
        exit(-1)

time.sleep(1)
send_data(bp, "N;0;255;255;255\n", serial)
time.sleep(1)
send_data(bp, "N;0;000;255;000\n", serial)
time.sleep(1)
send_data(bp, "N;0;255;000;000\n", serial)
time.sleep(1)
send_data(bp, "N;0;000;000;255\n", serial)
time.sleep(1)